/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import cl.ciisa.ic201iecireol.ev02davidbousquetdao.dao.RegistroalumnosJpaController;
import cl.ciisa.ic201iecireol.ev02davidbousquetdao.entity.Registroalumnos;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author davidbousquet
 */
@WebServlet(name = "RegistroAlumnoController", urlPatterns = {"/RegistroAlumnoController"})
public class RegistroAlumnoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistroAlumnoController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistroAlumnoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RegistroalumnosJpaController dao = new RegistroalumnosJpaController();
        String action = request.getParameter("action");

        if (action.equals("nuevoAlumno")) {
            try {
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String carrera = request.getParameter("carrera");
                String correo = request.getParameter("correo");

                Registroalumnos registroAlumno = new Registroalumnos();
                registroAlumno.setRut(rut);
                registroAlumno.setNombre(nombre);
                registroAlumno.setApellido(apellido);
                registroAlumno.setCarrera(carrera);
                registroAlumno.setCorreo(correo);

                dao.create(registroAlumno);

            } catch (Exception ex) {
                System.out.println(ex.toString());
            }

        }

        if (action.equals("eliminar")) {
            try {
                Short id = Short.parseShort(request.getParameter("id"));
                dao.destroy(id);
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
        if (action.equals("guardar")) {
            try {
                Short id = Short.parseShort(request.getParameter("id"));
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String carrera = request.getParameter("carrera");
                String correo = request.getParameter("correo");

                Registroalumnos registroAlumno = new Registroalumnos();
                registroAlumno.setId(id);
                registroAlumno.setRut(rut);
                registroAlumno.setNombre(nombre);
                registroAlumno.setApellido(apellido);
                registroAlumno.setCarrera(carrera);
                registroAlumno.setCorreo(correo);

                dao.edit(registroAlumno);

            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }

        if (action.equals("editar")) {
            try {
                Short id = Short.parseShort(request.getParameter("id"));
                Registroalumnos registroAlumno = dao.findRegistroalumnos(id);
                request.setAttribute("registroAlumno", registroAlumno);
                request.getRequestDispatcher("editar.jsp").forward(request, response);
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }

        List<Registroalumnos> listaRegistros = dao.findRegistroalumnosEntities();
        request.setAttribute("listaRegistros", listaRegistros);
        request.getRequestDispatcher("listar.jsp").forward(request, response);

        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
