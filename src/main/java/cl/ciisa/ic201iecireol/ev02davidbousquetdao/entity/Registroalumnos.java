/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.ic201iecireol.ev02davidbousquetdao.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Size;

/**
 *
 * @author davidbousquet
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Registroalumnos.findAll", query = "SELECT r FROM Registroalumnos r"),
    @NamedQuery(name = "Registroalumnos.findById", query = "SELECT r FROM Registroalumnos r WHERE r.id = :id"),
    @NamedQuery(name = "Registroalumnos.findByRut", query = "SELECT r FROM Registroalumnos r WHERE r.rut = :rut"),
    @NamedQuery(name = "Registroalumnos.findByNombre", query = "SELECT r FROM Registroalumnos r WHERE r.nombre = :nombre"),
    @NamedQuery(name = "Registroalumnos.findByApellido", query = "SELECT r FROM Registroalumnos r WHERE r.apellido = :apellido"),
    @NamedQuery(name = "Registroalumnos.findByCarrera", query = "SELECT r FROM Registroalumnos r WHERE r.carrera = :carrera"),
    @NamedQuery(name = "Registroalumnos.findByCorreo", query = "SELECT r FROM Registroalumnos r WHERE r.correo = :correo")})
public class Registroalumnos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Short id;
    @Size(max = 2147483647)
    private String rut;
    @Size(max = 2147483647)
    private String nombre;
    @Size(max = 2147483647)
    private String apellido;
    @Size(max = 2147483647)
    private String carrera;
    @Size(max = 2147483647)
    private String correo;

    public Registroalumnos() {
    }

    public Registroalumnos(Short id) {
        this.id = id;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registroalumnos)) {
            return false;
        }
        Registroalumnos other = (Registroalumnos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.ciisa.ic201iecireol.ev02davidbousquetdao.entity.Registroalumnos[ id=" + id + " ]";
    }
    
}
