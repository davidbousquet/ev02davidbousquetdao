<%-- 
    Document   : editar
    Created on : Apr 25, 2021, 9:23:04 PM
    Author     : davidbousquet
--%>

<%@page import="cl.ciisa.ic201iecireol.ev02davidbousquetdao.entity.Registroalumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Registroalumnos registroAlumno = (Registroalumnos) request.getAttribute("registroAlumno");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <title>Editar Alumno - Evaluación 02 - TALLER DE APLICACIONES EMPRESARIALES - IC201IECIREOL</title>
    </head>
    <body>
        <div class="container">
            <div class="row text-center">
                <h1>Editar Alumno</h1>
            </div>
            <div class="row">
                <div class="col">
                    <form action="RegistroAlumnoController" method="POST">
                        <input hidden="true" name="id" value="<%= registroAlumno.getId()%>"/>
                        <div class="mb-3">
                            <label for="rut" class="form-label">Rut</label>
                            <input type="text" class="form-control" name="rut" value="<%= registroAlumno.getRut()%>">
                        </div>
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre" value="<%= registroAlumno.getNombre()%>">
                        </div>
                        <div class="mb-3">
                            <label for="apellido" class="form-label">Apellido</label>
                            <input type="text" class="form-control" name="apellido" value="<%= registroAlumno.getApellido()%>">
                        </div>
                        <div class="mb-3">
                            <label for="correo" class="form-label">Email</label>
                            <input type="email" class="form-control" name="correo" value="<%= registroAlumno.getCorreo()%>">
                        </div>
                        <div class="mb-3">
                            <label for="carrera" class="form-label">Carrera</label>
                            <input type="text" class="form-control" name="carrera" value="<%= registroAlumno.getCarrera()%>">
                        </div>

                        <button name="action" value="guardar" class="btn btn-lg btn-primary">Guardar Cambios</button>
                        <button name="action" value="listarAlumnos" class="btn btn-lg btn-warning">Cancelar</button>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
