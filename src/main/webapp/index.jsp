<%--
    Document   : index
    Created on : Apr 25, 2021, 6:31:58 PM
    Author     : davidbousquet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <title>Registro Alumnos - Evaluación 02 - TALLER DE APLICACIONES EMPRESARIALES - IC201IECIREOL</title>
    </head>
    <body>
        <div class="container">
            <div class="row text-center">
                    <h1>Registro Alumnos</h1>
            </div>
            <div class="row">
                <div class="col">
                    <form action="RegistroAlumnoController" method="POST">
                        <div class="mb-3">
                            <label for="rut" class="form-label">Rut</label>
                            <input type="text" class="form-control" name="rut">
                        </div>
                        <div class="mb-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre">
                        </div>
                        <div class="mb-3">
                            <label for="apellido" class="form-label">Apellido</label>
                            <input type="text" class="form-control" name="apellido">
                        </div>
                        <div class="mb-3">
                            <label for="correo" class="form-label">Email</label>
                            <input type="email" class="form-control" name="correo">
                        </div>
                        <div class="mb-3">
                            <label for="carrera" class="form-label">Carrera</label>
                            <input type="text" class="form-control" name="carrera">
                        </div>

                        <button name="action" value="nuevoAlumno" class="btn btn-lg btn-primary">Registrar</button>
                        <button name="action" value="listarAlumnos" class="btn btn-lg btn-success">Ver Registros</button>
                        
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
