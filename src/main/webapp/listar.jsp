<%-- 
    Document   : listar
    Created on : Apr 25, 2021, 7:49:33 PM
    Author     : davidbousquet
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.ciisa.ic201iecireol.ev02davidbousquetdao.entity.Registroalumnos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Registroalumnos> alumnos = (List<Registroalumnos>) request.getAttribute("listaRegistros");
    Iterator<Registroalumnos> iterador = alumnos.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

        <title>Alumnos Registrados - Evaluación 02 - TALLER DE APLICACIONES EMPRESARIALES - IC201IECIREOL</title>
    </head>
    <body>
        <div class="container">
            <div class="row text-center">
                <h1>Alumnos Registrados</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Acciones</th>
                                <th scope="col">Rut</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellido</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Carrera</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% while (iterador.hasNext()) {
                                    Registroalumnos registroAlumno = iterador.next();%>
                            <tr>
                                <td>
                                    <form action="RegistroAlumnoController" method="POST" >
                                        <input hidden="true" name="id" value="<%= registroAlumno.getId()%>"/>
                                        <button name="action" value="eliminar" class="btn btn-danger">
                                            X
                                        </button>
                                        <button name="action" value="editar" class="btn btn-warning">
                                            Editar
                                        </button>
                                    </form>
                                </td>
                                <td><%= registroAlumno.getRut()%></td>
                                <td><%= registroAlumno.getNombre()%></td>
                                <td><%= registroAlumno.getApellido()%></td>
                                <td><%= registroAlumno.getCorreo()%></td>
                                <td><%= registroAlumno.getCarrera()%></td>
                            </tr>

                            <% }%>


                        </tbody>

                    </table>
                </div>
                <div class="col-12">
                    <a href="index.jsp"class="btn btn-lg btn-success">Registrar Nuevo</a>
                </div>
            </div>
        </div>
    </body>
</html>
